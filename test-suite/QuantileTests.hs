{-# LANGUAGE ScopedTypeVariables #-}

module QuantileTests where

import Quantile
    ( sumQuantiles
    , sumQuantile
    , toEnglishOrdinal )

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.QuickCheck.Modifiers
import Test.QuickCheck.Instances

import qualified Data.Vector.Unboxed          as U

unit_sumQuantiles :: IO ()
unit_sumQuantiles =
    sumQuantiles 5 (U.fromList [1..10]) @?= [3.0, 7.0, 11.0, 15.0, 19.0]

prop_sumQuantiles :: Positive Int -> U.Vector Double -> Property
prop_sumQuantiles (Positive nQ)
                  xs
  =   nQ > 1
  ==> length (sumQuantiles nQ xs) == nQ

unit_sumQuantile :: IO ()
unit_sumQuantile =
    sumQuantile 1 2 (U.fromList [1..10]) @?= 15

prop_sumQuantile :: Positive Int -> Positive Int -> U.Vector Double -> Property
prop_sumQuantile (Positive n)
                 (Positive nQ)
                 xs
  =   nQ > 1 && n <= nQ
  ==> (sumQuantile n nQ xs) * 0 == 0

unit_toEnglishOrdinal :: IO ()
unit_toEnglishOrdinal =
    toEnglishOrdinal 11 @?= "11th"

prop_toEnglishOrdinal :: Positive Int -> Property
prop_toEnglishOrdinal (Positive n)
  =   n > 0
  ==> length (toEnglishOrdinal n) == length (show n) + 2
