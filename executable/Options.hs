module Options
  ( Options(..)
  , Input(..)
  , withOptions
  ) where

import Options.Applicative
import Data.Semigroup ((<>))

data Options = Options Input Int

data Input
  = FileInput FilePath
  | StdInput

withOptions :: (Options -> IO ()) -> IO ()
withOptions f = f =<< execParser opts
  where
    opts = info (helper <*> parseOptions)
      ( fullDesc
     <> progDesc "Calculate quantiles and basic visualizations"
     <> header "quantiles - quick stats for the command line" )

parseOptions :: Parser Options
parseOptions = Options
            <$> parseInput
            <*> option auto
                ( long "quantiles"
               <> short 'n'
               <> showDefault
               <> value 5
               <> help "Number of quantiles to calculate"
               <> metavar "INT" )

parseInput :: Parser Input
parseInput = parseFileInput <|> parseStdInput

parseFileInput :: Parser Input
parseFileInput = FileInput <$> strOption
  (  long "file"
  <> short 'f'
  <> metavar "FILENAME"
  <> help "Read from input file" )

parseStdInput :: Parser Input
parseStdInput = flag' StdInput
  (  long "stdin"
  <> help "Read from stdin" )
