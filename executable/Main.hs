module Main (main) where

import           System.IO
import qualified Data.Vector.Unboxed          as U

import           Options
import qualified Quantile

main :: IO ()
main = withOptions $ run

run :: Options -> IO ()
run (Options input qN) = do
    ns <- readNumbers input
    putStrLn $ showInputLength filename ns
    Quantile.report ns qN
  where
  filename = case input of
    FileInput f -> f
    StdInput    -> "stdin"

readNumbers :: Input -> IO (U.Vector Double)
readNumbers (FileInput i) = openFile i ReadMode >>= hGetContents >>= return . getNumbers . lines
readNumbers StdInput = getContents >>= return . getNumbers . lines

getNumbers :: [String] -> U.Vector Double
getNumbers xs = U.fromList (map (\n -> read n :: Double) xs)

-- Rename or change to IO ()
showInputLength :: String -> U.Vector Double -> String
showInputLength src xs = "Reading input: " ++ show (U.length xs) ++ " values from " ++ src
