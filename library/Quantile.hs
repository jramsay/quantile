module Quantile
    ( quantile
    , report
    , sumQuantiles
    , sumQuantile
    , toEnglishOrdinal
    ) where

-- Statistics
import           Statistics.Quantile          (quantiles, spss)
-- Vectors
import           Data.Vector.Algorithms.Merge as Merge
import qualified Data.Vector.Unboxed          as U
-- Tabular output
import qualified Text.PrettyPrint.Boxes       as B
import           Text.Printf

quantile :: ()
quantile = ()

report :: U.Vector Double -> Int -> IO ()
report ns qN = do
  let total = U.sum ns
  let qs = quantiles spss [0..qN] qN ns
  let qSums = sumQuantiles qN ns
  let chart = constructHorizontalChart 50 qSums
  putStrLn $ "Sum: " ++ show total
  printTable [
      -- Column: ordinal labels
      (B.right, map toEnglishOrdinal [1..qN]),
      -- Column: quantile maximum
      (B.right, map (printf "%.2f") (tail qs)),
      -- Column: sum
      (B.right, map (printf "%.2f") qSums),
      -- Column: sum percent
      (B.right, map (printf "%.2f%%") (map (\x -> x / total * 100) qSums)),
      (B.left,  chart)
    ]

-- Ordinal labels for quantiles like 1st, 2nd, 3rd etc
toEnglishOrdinal :: Int -> String
toEnglishOrdinal n
    | n < 0     = show n
    | otherwise = show n ++ suffix
  where
  suffix = case n of
    -- English exceptions of exceptions
    11 -> "th"
    12 -> "th"
    13 -> "th"
    _ -> case mod n 10 of
      1 -> "st"
      2 -> "nd"
      3 -> "rd"
      _ -> "th"

-- Table is constructed column wise
printTable :: [(B.Alignment, [String])] -> IO ()
printTable columns = B.printBox $ B.hsep 2 B.left $ map alignColumn columns
  where
  alignColumn c = B.vcat align (map B.text values)
    where (align, values) = c

sumQuantiles :: Int -> U.Vector Double -> [Double]
sumQuantiles nQ xs
  | nQ < 2    = error "sumQuantiles: At least 2 quantiles is needed"
  | U.null xs = replicate nQ 0.0
  | otherwise = map (\k -> sumQuantile k nQ sortedXs) [1..nQ]
  where
    sortedXs = U.modify Merge.sort xs

-- Quartile sum
-- - n: quartile number
-- - nQ: number of quartiles
-- - xs: input data, assumed sorted
sumQuantile :: Int -> Int -> U.Vector Double -> Double
sumQuantile n nQ xs
  | nQ < 2    = error "sumQuantile: At least 2 quantiles is needed"
  | U.null xs = 0.0
  | otherwise = (U.sum sliceXs) - headOverhang - tailOverhang
  where
    size     = fromIntegral (U.length xs)
    x        = (fromIntegral n - 1) / (fromIntegral nQ) * size :: Double
    y        = (fromIntegral n) / (fromIntegral nQ) * size :: Double
    sliceL   = ceiling y - floor x
    sliceXs  = U.slice (floor x) sliceL xs
    headOverhang = U.head sliceXs * (x - fromIntegral (floor x :: Int))
    tailOverhang = U.last sliceXs * (fromIntegral (ceiling y :: Int) - y)


-- Unicode 1/8th block characters
unicodeBlocks :: [Char]
unicodeBlocks = ['▏','▎','▍','▌','▋','▊','▉','█']

constructHorizontalBar :: Int -> Int -> Double -> String
constructHorizontalBar maxValue maxWidth value = concat [
    replicate fullBlocks (last unicodeBlocks),
    [unicodeBlocks !! partialBlocks]
  ]
  where
    points        = length unicodeBlocks
    widthInPoints = fromIntegral maxWidth * points
    barInPoints   = round value * widthInPoints `div` maxValue
    fullBlocks    = barInPoints `div` points
    partialBlocks = barInPoints `mod` points

constructHorizontalChart :: Int -> [Double] -> [String]
constructHorizontalChart width xs = map (constructHorizontalBar maxValue width) xs
  where
    maxValue = ceiling (maximum xs)
